import { Injectable } from '@angular/core';
import { Bolo } from './bolo';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetbolosService {

  constructor(private angularFirestore: AngularFirestore) { }

  meusBolos(): Bolo[] {
    return [
      {
        id: "1",
        nome: 'Bolo maquiado',
        descricao: 'Quem não é transportado de volta para a infância quando come aquele bolo delicioso que a avó fazia? Só de imaginar o cheirinho do bolo saindo do forno, aquele sentimento de saudades e nostalgia domina qualquer um',
        img: '../../assets/pictures/img1.jpeg',
        preco: 22.30
      },
      {
        id: "2",
        nome: 'Bolo formigueiro',
        descricao: 'Quem não é transportado de volta para a infância quando come aquele bolo delicioso que a avó fazia? Só de imaginar o cheirinho do bolo saindo do forno, aquele sentimento de saudades e nostalgia domina qualquer um',
        img: '../../assets/pictures/img3.jpeg',
        preco: 19.60
      },
      {
        id: "3",
        nome: 'Bolo diabético',
        descricao: 'Quem não é transportado de volta para a infância quando come aquele bolo delicioso que a avó fazia? Só de imaginar o cheirinho do bolo saindo do forno, aquele sentimento de saudades e nostalgia domina qualquer um',
        img: '../../assets/pictures/img2.jpeg',
        preco: 12.99
      },
      {
        id: "4",
        nome: 'Bolo flor',
        descricao: 'Quem não é transportado de volta para a infância quando come aquele bolo delicioso que a avó fazia? Só de imaginar o cheirinho do bolo saindo do forno, aquele sentimento de saudades e nostalgia domina qualquer um',
        img: '../../assets/pictures/img5.jpeg',
        preco: 35
      },
      {
        id: "5",
        nome: 'Bolo de nutela',
        descricao: 'Quem não é transportado de volta para a infância quando come aquele bolo delicioso que a avó fazia? Só de imaginar o cheirinho do bolo saindo do forno, aquele sentimento de saudades e nostalgia domina qualquer um',
        img: '../../assets/pictures/img4.jpeg',
        preco: 27.30
      }
    ];
  }

  uploadServidor() {
    this.meusBolos().forEach(bolo => {
      this.angularFirestore.collection<Bolo>('bolos').add(bolo);
    })
  }

  getBoloById(id: string): Observable<Bolo> {
    return this.angularFirestore.collection<Bolo>('bolos').doc(id).get().pipe(
      map(actions => {
        const data = actions.data();
        const id = actions.id;
        return { id, ...data }
      })
    )
  }

  getBolosServidor(): Observable<Bolo[]> {
    return this.angularFirestore.collection<Bolo>('bolos').snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id_firebase = a.payload.doc.id;
          return { id_firebase, ...data };
        })
      })
    )
  }

  getCarrinhoServidor(): Observable<Bolo[]> {
    return this.angularFirestore.collection<Bolo>('carrinho').snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id_firebase = a.payload.doc.id;
          return { id_firebase, ...data };
        })
      })
    )
  }

  addCarrinho(bolo) {
    return this.angularFirestore.collection<Bolo>('carrinho').add(bolo);
  }

  finalizaPedido(carrinho: Bolo[]) {
    carrinho.forEach(bolo => {
      this.angularFirestore.collection<Bolo>('carrinho').doc(bolo['id_firebase']).delete();
    });
  }

}
