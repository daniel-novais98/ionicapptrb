import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'paginapedido', loadChildren: './paginapedido/paginapedido.module#PaginapedidoPageModule' },
  { path: 'paginapedido/:id', loadChildren: './paginapedido/paginapedido.module#PaginapedidoPageModule' },
  { path: 'paginapagamento', loadChildren: './paginapagamento/paginapagamento.module#PaginapagamentoPageModule' },
  { path: 'paginapagamento/:id', loadChildren: './paginapagamento/paginapagamento.module#PaginapagamentoPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
