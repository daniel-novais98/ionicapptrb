import { Component, OnInit } from '@angular/core';
import { Bolo } from '../bolo';
import { GetbolosService } from '../getbolos.service';
import { LoadingController, NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-paginapagamento',
  templateUrl: './paginapagamento.page.html',
  styleUrls: ['./paginapagamento.page.scss'],
})
export class PaginapagamentoPage implements OnInit {

  bolos: Bolo[] = [];
  loading: any;

  constructor(private navContrl: NavController, private getBolosService: GetbolosService, private loadingController: LoadingController, private toastController: ToastController) {
    this.getBolosService.getCarrinhoServidor().subscribe((carrinho) => {
      this.bolos = carrinho;
    });
  }

  ngOnInit() {
  }

  async confirmarPedido() {
    this.getBolosService.finalizaPedido(this.bolos);
    await this.presentLoading();
    await this.navContrl.navigateRoot('home');
    this.loading.dismiss().then(() => {
      this.presentToast("Pedido finalizado com sucesso");
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Por favor, aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }
}
