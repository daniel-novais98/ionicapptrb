import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginapagamentoPage } from './paginapagamento.page';

describe('PaginapagamentoPage', () => {
  let component: PaginapagamentoPage;
  let fixture: ComponentFixture<PaginapagamentoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginapagamentoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginapagamentoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
