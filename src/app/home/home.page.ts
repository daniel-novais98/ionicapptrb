import { Component } from '@angular/core';
import {Bolo} from '../bolo';
import {GetbolosService} from '../getbolos.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  bolos: Bolo[] = [];

  constructor(private getBolosService: GetbolosService) {
    this.getBolosService.getBolosServidor().subscribe((bolos) => {
      this.bolos = bolos;
    })
  }


}
